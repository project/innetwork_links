<?php

/**
 * @file
 * Admin settings for in-network links.
 */

/**
 * Settings form.
 */
function innetwork_links_settings($form, &$form_state) {
  $form = array();
  $form['innetwork_links_links'] = array(
    '#type' => 'textarea',
    '#title' => t('In-network links'),
    '#description' => t('Enter one link per line. These are the links which can only be accessed within your network. Enter each in the format google.com (no http, no www, no trailing slash).'),
    '#default_value' => variable_get('innetwork_links_links', ''),
  );
  $form['innetwork_links_redirect_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect path'),
    '#description' => t('Enter a URL to redirect out-of-network users to when they click an in-network link. This can be a full URL beginning with http or a relative path beginning with /, e.g. http://google.com or /node/5'),
    '#default_value' => variable_get('innetwork_links_redirect_path', ''),
  );
  $form['innetwork_links_class'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Class'),
    '#description' => t('Enter a class e.g. redirect-link to be added to the in-network links for users out of the network.'),
    '#default_value' => variable_get('innetwork_links_class', ''),
  );
  $form['innetwork_links_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Link title'),
    '#description' => t('Enter a title to be added to in-network links for users out of the network. This will appear on link hover.'),
    '#default_value' => variable_get('innetwork_links_title', ''),
  );
  $form['innetwork_links_ips'] = array(
    '#type' => 'textarea',
    '#title' => t('In-network IPs'),
    '#description' => t('Enter one IP address per line. These are the IP Addresses which qualify as within your network. Users outside these IPs will be redirected when clicking an in-network link.'),
    '#default_value' => variable_get('innetwork_links_ips', ''),
  );
  return system_settings_form($form);
}



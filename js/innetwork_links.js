(function ($) {
  Drupal.behaviors.innetwork_links = {
    attach: function(context, settings) {
      var innetwork_links = settings.innetwork_links;
      $("#page a:not(.innetworklinks-processed)").each(function() {
        for (key in innetworklinks.links) {
          if ($(this).attr('href') != undefined && $(this).attr('href').indexOf(innetworklinks.links[key]) !== -1) {
            if (innetworklinks.redirect_path != '') {
              this.href = innetworklinks.redirect_path;
            }
            if (innetworklinks.add_class != '') {
              $(this).addClass(innetworklinks.add_class);
            }
            if (innetworklinks.title != '') {
              $(this).attr('title', innetworklinks.title);
            }
            $(this).addClass('innetworklinks-processed');
          }
        }
      });
    }
  }
}(jQuery));
